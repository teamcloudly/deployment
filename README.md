# Team Cloudly
## GCP Final 'Big' Homework
by Geoffrey Clark

## Overview
We (Team Cloudly) wanted to create an organizational structure that could be created, torn down, and re-created in one line. Ideally our goal was to demo for the presentation something like `cft apply *.yaml` which would create folders, projects, the shared vpc, and compute instances running webservers serving static content. I don't think at the time we realized how ambitious this goal was. 

## CFT CLI
It took a bit of time to learn CFT and pretty much all milestones were challenging. The first thing we did was install CFT CLI on a Compute Instance running Ubuntu as some of our team members were on Windows. From there we copied relevant templates from the master branch of the CFT repository; folders, projects, compute instance, firewall rules and iam rules and tried to use them. 

### Successes
- Creating folders
- Creating projects with folder references using out variables
- Creating a sharedVPC host project using the project template
- Creating service projects to connect to sharedVPC
- Creating compute engine resources in service projects that install Apache with startup script, and overwrite default landing page with 'Hello world' that includes instance name (done with single instance template python wrapper that reads instance name from .yaml)
- Created firewall rule (tcp:80 allow ingress 0.0.0.0/0) on host VPC project
- Connecting to webserver from public internet, only possible via firewall rule
o
### Challenges
- $(out) referenecs (first used with project) were very confusing and quickly forgotten. I spent hours trying to do this and was only able to solve it after Adam (Google CFT CLI Expert) provided a solution. I then tried to repeat this pattern a couple weeks later and had totally forgotten
- You can't paramterize which project to run the resource templates in and this has to be hard-coded. This leads to tier a) repeating entire templates (for duplicate resources, in this case a compute engine) or b) using sed or other tools to find/replace a single template file (see create-environ.sh). Either solution seems like an anti-pattern. 
- The networkAccess: true rule didn't seem to behave as expected. To add compute engine instances to the host vpc from a service vpc project the deployment-manager service account of the service project needs network user IAM permissions on the host project. This has to be done after the sharedVPC service project is created but before the compute engine is created. I'm not sure that CFT provides this functionality (and if so, no group to my knowledge was successfully able to leverage it).
- Impossible to do most of the above successes in one step. Necessary to run CFT for individual steps, such as granting the required network access IAM rule or firewall rule. 
- Learning to use CFT and debugging was slow as documentation and examples are lacking (most templates appear to be  stock dm templates and don't leverage cft)

