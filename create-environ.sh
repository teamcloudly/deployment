#!/bin/bash

export PROJECT_ID=p01-prod
ORG_ENV=prod
INSTANCE_NAME=prod-web-server
FOLDER_ID=organic-prod

sed "s/p03-web-app/$PROJECT_ID/" p03-web-app.yaml > "$PROJECT_ID".yaml
sed -i "s/test/$ORG_ENV/" "$PROJECT_ID".yaml

cft apply "$PROJECT_ID".yaml 

PROJECT_NUMBER=$(gcloud projects list | grep "$PROJECT_ID".yaml | awk '{print $3}')
# echo "$PROJECT_NUMBER"

NU_YAML=infras-"$PROJECT_ID"-networkUser.yaml
# sed "s/p03-web-app/$PROJECT_ID/" infras-p03-networkUser.yaml > infras-"$PROJECT_ID"-networkUser.yaml
sed "s/p03-web-app/$PROJECT_ID/" infras-p03-networkUser.yaml > $NU_YAML
sed -i "s/126035872148/$PROJECT_NUMBER/" $NU_YAML
cft apply $NU_YAML

SERVER_YAML="$PROJECT_ID"-web-server.yaml
sed "s/p05-dev-service/$PROJECT_ID/" dev-web-server.yaml > $SERVER_YAML
sed -i "s/dev/$ORG_ENV/" $SERVER_YAML

cft apply $SERVER_YAML

