
def generate_config(context):

    print 'project-wrapper context: {} \n'.format(context)
    print 'folders: {}'.format(context.properties['folders'])
    print 'project-wrapper context.env: {} \n'.format(context.env)
    print 'project-wrapper context.properties: {} \n'.format(context.properties)

    context.properties['parent']['id'] = context.properties["folders"][context.properties["folderName"]]["name"][8:]

    # Set some defaults on the service account
    context.properties['serviceAccounts'] = [{
       'displayName': 'Project {} SA'.format(context.env['name'].split('-')[0][1:]),
       'networkAccess': True,
       'accountId': '{}-default-sa'.format(context.env['name'].split('-')[0]),
       'roles': [
            'roles/editor',
            'roles/viewer'
            ]
        }]

    return {
        'resources': [{
            'type': "cft-project.py",
            'name': context.env['name'],
            'properties': context.properties}]
    }
