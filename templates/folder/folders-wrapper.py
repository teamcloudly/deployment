
def generate_config(context):
    print 'folder context: {} \n'.format(context.properties['folders'])
    # Using some global values from an external config file.
    # Hardcoded for this example.

#   global_prefix = "Organic "

    # Manipulate context.properties #

    outputs = []

    for folder in context.properties["folders"]:
        print 'folder: {} \n'.format(folder)
#       outputs.append({folder['displayName']: folder})
#       outputs.append({folder['displayName']: folder})
#       outputs.append({folder['name']: folder})
#       if 'DEV' in folder['displayName']: outputs.append({'dev-folder-id': folder['folderId']})

    # Passing values forward to CFT template

    print 'folder outputs: {} \n'.format(outputs)

    return {
        'resources': [{
            'type': "cft-folder.py",
            'name': context.env['name'],
            'properties': context.properties}],
        'outputs': outputs
        
    }
