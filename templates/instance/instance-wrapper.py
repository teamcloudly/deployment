
def generate_config(context):
        print 'instance context: {}\n'.format(context.properties)
        
#       context.properties['metadata']['items']['value'] = '{} && echo "Hello from {}"'.format(context.properties['metadata']['items']['value'], context.env['name']) 

        for item in context.properties['metadata']['items']:
                if item['key'] == 'startup-script': 
                        item['value'] = '{} && echo "Hello from {}" > /var/www/html/index.html'.format(item['value'], context.env['name'])

#       raise Exception(context.properties)

        return {
                'resources': [{
                        'type': "cft-instance.py",
                        'name': context.env['name'],
                        'properties': context.properties}]
        }

